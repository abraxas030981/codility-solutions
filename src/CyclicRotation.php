<?php

class CyclicRotation
{
    public function rotate(array $array, $numberOfRotations): array
    {
        $result = [];

        $arrayLength = count($array);

        foreach ($array as $key => $value) {

            $result[($key + $numberOfRotations) % $arrayLength] = $value;

        }

        return $result;
    }

}