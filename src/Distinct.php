<?php

class Distinct
{
    public function countDistinct(array $array): int
    {
        $result = [];

        foreach ($array as $value) {

            $result[$value] = true;

        }

        return count($result);
    }

}