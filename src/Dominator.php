<?php

class Dominator
{

    public function find(array $arr): Int
    {
        $halfArrayCount = count($arr) / 2;
        $tempArr = [];

        foreach ($arr as $key => $value) {

            if (!isset($tempArr[$value])) {
                $tempArr[$value] = 1;
            } else {
                $tempArr[$value]++;
            }

            if ($tempArr[$value] > $halfArrayCount) {
                return $key;
            }

        }

        return -1;
    }

}