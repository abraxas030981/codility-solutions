<?php

class CountFactors
{
    public function count(Int $int): int
    {
        $factors = 0;

        $factorList = [];

        for ($i = 1; $i <= floor(sqrt($int)); $i++) {

            if ($int % $i === 0) {
                $factorList[] = $i;
                $factorList[] = $int / $i;
                $factors += 2;
            }
        }

        return $factors;
    }

}