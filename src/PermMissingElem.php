<?php

class PermMissingElem
{
    public function findMissingElement(array $arr): int
    {
        $arrayLength = count($arr) + 1;

        $goal = ($arrayLength * ($arrayLength + 1)) / 2;

        $actualSum = array_sum($arr);

        return $goal - $actualSum;

    }
}