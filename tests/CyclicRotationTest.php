<?php
/**
 * Created by PhpStorm.
 * User: smuhtic
 * Date: 9.2.2018.
 * Time: 12:05
 */

namespace Tests;

use CyclicRotation;
use PHPUnit\Framework\TestCase;

class CyclicRotationTest extends TestCase
{

    protected $cyclicRotation;

    public function setUp()
    {
        //Arrange
        $this->cyclicRotation = new CyclicRotation();
    }

    /**
     * @dataProvider provideData
     */
    public function testRotate($arr, $numberOfRotations, $expected)
    {
        //Act
        $result = $this->cyclicRotation->rotate($arr, $numberOfRotations);
        //Assert
        self::assertEquals($expected, $result);
    }

    public function provideData()
    {
        return [
            [[3, 8, 9, 7, 6], 3, [9, 7, 6, 3, 8]],
            [[3, 8, 9, 7, 6], 0, [3, 8, 9, 7, 6]],
            [[3, 8, 9, 7, 6], 5, [3, 8, 9, 7, 6]],
        ];

    }


}
