<?php
/**
 * Created by PhpStorm.
 * User: smuhtic
 * Date: 9.2.2018.
 * Time: 12:05
 */

namespace Tests;

use CountFactors;
use PHPUnit\Framework\TestCase;

class CountFactorsTest extends TestCase
{

    protected $countFactors;

    public function setUp()
    {
        //Arrange
        $this->countFactors = new CountFactors();
    }

    /**
     * @dataProvider provideData
     */
    public function testFactors($int, $expected)
    {
        //Act
        $result = $this->countFactors->count($int);
        //Assert
        self::assertEquals($expected, $result);
    }

    public function provideData()
    {
        return [
            [24, 8],
            [30, 8],
            [10, 4],
            [96, 12],
        ];

    }


}
