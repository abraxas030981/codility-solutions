<?php
/**
 * Created by PhpStorm.
 * User: smuhtic
 * Date: 9.2.2018.
 * Time: 12:58
 */

namespace Tests;

use Distinct;
use PHPUnit\Framework\TestCase;

class DistinctTest extends TestCase
{

    protected $distinct;

    public function setUp()
    {
        //Arrange
        $this->distinct = new Distinct();
    }


    /**
     * @dataProvider provideData
     */
    public function testDistinctValues($arr, $expected)
    {
        //Act
        $result = $this->distinct->countDistinct($arr);

        //Assert
        self::assertEquals($expected, $result);
    }


    public function provideData()
    {
        return [
            [[2, 1, 1, 2, 3, 1], 3]
        ];

    }
}
