<?php
/**
 * Created by PhpStorm.
 * User: smuhtic
 * Date: 12.2.2018.
 * Time: 8:54
 */

namespace Tests;

use Dominator;
use PHPUnit\Framework\TestCase;

class DominatorTest extends TestCase
{

    protected $dominator;

    public function setUp()
    {
        //Arrange
        $this->dominator = new Dominator();
    }


    /**
     * @dataProvider provideNumbers
     */
    public function testFind($arr, $expected)
    {
        //Act
        $result = $this->dominator->find($arr);

        //Assert
        self::assertContains($result, $expected);
    }

    public function provideNumbers()
    {
        return [
            [[3, 4, 3, 2, 3, -1, 3, 3], [0, 2, 4, 6, 7]],
            [[3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4], [5, 6, 7, 8, 9, 10, 11, 12]],
            [[1, 4, 4], [1, 2]],
            [[1, 1, 4, 4], [-1]],
        ];
    }
}
