<?php
/**
 * Created by PhpStorm.
 * User: smuhtic
 * Date: 9.2.2018.
 * Time: 9:58
 */

namespace Tests;

use PermMissingElem;
use PHPUnit\Framework\TestCase;

class PermMissingElemTest extends TestCase
{

    protected $permMissingElement;

    public function setUp()
    {
        //Arrange
        $this->permMissingElement = new PermMissingElem();
    }

    /**
     * @dataProvider provideArray
     */
    public function testFindMissingElement($arr, $expected)
    {
        //Act
        $result = $this->permMissingElement->findMissingElement($arr);
        //Assert
        self::assertEquals($expected, $result);

    }

    public function provideArray()
    {
        return [
            [[2, 3, 1, 5], 4],
            [[2, 4, 1, 5], 3],
            [[4, 3, 1, 5], 2],
            [[2, 3, 4, 5], 1],
            [[2, 3, 1, 4], 5]
        ];
    }
}
